use std::fs;
use std::mem::{self, MaybeUninit};
use regex::Regex;

macro_rules! coord {
    ($row_size:expr, $row:expr, $column:expr) => { ($row * $row_size) + $column };
    ($row_size:expr, $value:expr) => { ($value / $row_size, ($value % $row_size))};
}

struct Number<'a> {
    number: &'a [u8],
    pos: usize,
}

impl<'a> Number<'a> {
    fn isolated(&self, map: &Vec<u8>, size: (usize, usize)) -> bool {
	// begining
	let beg = coord!(size.0, self.pos);
	// ↑
	if beg.0 != 0 && map[coord!(size.0, beg.0 - 1, beg.1)] != b'.' {
	    return false;
	}
	// ↑←
	if beg.0 != 0 && beg.1 != 0 && map[coord!(size.0, beg.0 - 1, beg.1 - 1)] != b'.' {
	    return false;
	}
	// ←
	if beg.1 != 0 && map[coord!(size.0, beg.0, beg.1 - 1)] != b'.' {
	    return false;
	}
	// ↓←
	if beg.0 != size.0 - 1 && beg.1 != 0 && map[coord!(size.0, beg.0 + 1, beg.1 - 1)] != b'.' {
	    return false;
	}
	// ↓
	if beg.0 != size.0 - 1 && map[coord!(size.0, beg.0 + 1, beg.1)] != b'.' {
	    return false;
	}

	// end
	let end = coord!(size.0, self.pos + self.number.len() - 1);
	// ↑
	if end.0 != 0 && map[coord!(size.0, end.0 - 1, end.1)] != b'.' {
	    return false;
	}
	// ↑→
	if end.0 != 0 && end.1 != size.1 - 1 && map[coord!(size.0, end.0 - 1, end.1 + 1)] != b'.' {
	    return false;
	}
	// →
	if end.1 != size.1 - 1 && map[coord!(size.0, end.0, end.1 + 1)] != b'.' {
	    return false;
	}
	// ↓→
	if end.0 != size.0 - 1 && end.1 != size.1 - 1 && map[coord!(size.0, end.0 + 1, end.1 + 1)] != b'.' {
	    return false;
	}
	// ↓
	if end.0 != size.0 - 1 && map[coord!(size.0, end.0 + 1, end.1)] != b'.' {
	    return false;
	}

	// middle
	if self.number.len() <= 2 {
	    return true;
	}
	for i in 1..(self.number.len() - 1) {
	    let cell = coord!(size.0, self.pos + i);
	    // ↑
	    if cell.0 != 0 && map[coord!(size.0, cell.0 - 1, cell.1)] != b'.' {
		return false;
	    }
	    // ↓
	    if cell.0 != size.0 - 1 && map[coord!(size.0, cell.0 + 1, cell.1)] != b'.' {
		return false;
	    }
	}

	return true;
    }
}
	

fn map_size(input: &[u8]) -> (usize, usize) {
    let mut rows = 0;
    let mut columns = 0;
    for byte in input {
	if *byte == b'\n' {
	    rows += 1;
	}
	if rows == 0 {
	    columns += 1;
	}
    }
    return (rows, columns);
}
    
fn construct_map(input: &[u8], size: (usize, usize)) -> Vec<u8> {
    let rows = size.0;
    let columns = size.1;
    let cells = rows * columns;

    let mut map: Vec<MaybeUninit<u8>> = vec![unsafe {
	MaybeUninit::uninit().assume_init()
    }; cells];
    let mut cell = 0;
    for index in 0..input.len() {
	if input[index] == b'\n' {
	    continue;
	}
	map[cell].write(input[index]);
	cell += 1;
    }
    return unsafe {mem::transmute::<_, Vec<u8>>(map)};
}

fn find_numbers(map: &Vec<u8>) -> Vec<Number> {
    let mut numbers = Vec::new();
    let re = Regex::new(r"\d+").unwrap();
    for m in re.find_iter(std::str::from_utf8(&map[..]).unwrap()) {
	numbers.push(Number {number: m.as_str().as_bytes(), pos: m.start()});
    }
    return numbers;
}
    

fn main() {
    let content = fs::read_to_string("day03-input").unwrap();
    let bytes = content.as_bytes();
    let size = map_size(&bytes);
    let map = construct_map(&bytes, size);
    let numbers = find_numbers(&map);
    let mut sum = 0;
    for number in numbers {
	if !number.isolated(&map, size) {
	   sum += std::str::from_utf8(number.number).unwrap().parse::<u64>().unwrap()
	}
    }
    println!("Sum of parts: {}", sum);
}
