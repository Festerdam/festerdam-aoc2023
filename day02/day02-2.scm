(add-to-load-path (dirname (current-filename)))
(use-modules (day02-1))
(use-modules (srfi srfi-1))
(use-modules (ice-9 textual-ports))

(define (make-game-second line)
  (define game (make-game line))
  (define (dispatch action)
    (cond ((eq? action 'highest-color-value) highest-color-value)
	  ((eq? action 'get-power) get-power)
	  (else (game action))))
  (define (highest-color-value c)
    (fold (lambda (curr prev)
	    (if (equal? curr #f)
		prev
		(if (> curr prev)
		    curr
		    prev)))
	  0
	  (map (lambda (round)
		 (let ((entry (map-find ((round 'get-color-map)) c)))
		   (if (equal? entry #f)
		       #f
		       (cadr entry))))
	       ((game 'get-rounds)))))
  (define (get-power)
    (* (highest-color-value "red")
       (highest-color-value "green")
       (highest-color-value "blue")))
  dispatch)

(define (solution file-name)
  (fold + 0 (map (lambda (x) (((make-game-second x) 'get-power)))
		 (map string-trim-both
		      (string-split
		       (string-trim-both
			(call-with-input-file file-name get-string-all))
		       #\linefeed)))))

(display (solution "day02-input"))
(newline)
