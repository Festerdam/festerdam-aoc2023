(define-module (day02-1)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 textual-ports)
  #:export (make-game make-round map-find))

(define (make-game line)
  (define (dispatch action)
    (cond ((eq? action 'get-game-id) get-game-id)
	  ((eq? action 'valid?) valid?)
	  ((eq? action 'get-rounds) get-rounds)
	  (else (error (string-concatenate (list (symbol->string action)
						 " is not a valid action."))))))
  (define (get-round-strings)
    (map string-trim-both (cdr (string-split
				line
				(lambda (x) (or (eq? x #\:) (eq? x #\;)))))))
  (define (get-rounds)
    (map make-round (get-round-strings)))
  (define (get-game-id)
    (string->number (string-trim-right (cadr (string-split line #\space)) #\:)))
  (define (valid? r g b)
    (every (lambda (x) ((x 'valid?) r g b)) (get-rounds)))
  dispatch)

(define (make-round round)
  (define (dispatch action)
    (cond ((eq? action 'valid?) valid?)
	  ((eq? action 'get-color-map) get-color-map)
	  (else (error (string-concatenate (list (symbol->string action)
						 " is not a valid action."))))))
  (define (make-color-entry pair)
    (let ((split-pair (string-split pair #\space)))
      (list (cadr split-pair) (string->number (car split-pair)))))
  (define (get-color-map)
    (map make-color-entry (map string-trim-both (string-split round #\,))))
  (define (valid? r g b)
    (define (color-valid? c v)
      (let* ((color-map (get-color-map))
	     (entry (map-find color-map c)))
	(if (equal? entry #f)
	    #t
	    (>= v (cadr entry)))))
    (and (color-valid? "red" r)
	 (color-valid? "green" g)
	 (color-valid? "blue" b)))
  dispatch)

(define (map-find entries key) (find (lambda (x) (equal? key (car x))) entries))

(define (solution file-name)
  (fold + 0 (map (lambda (x) ((x 'get-game-id)))
		 (filter (lambda (x) ((x 'valid?) 12 13 14))
			 (map make-game
			      (map string-trim-both
				   (string-split
				    (string-trim-both
				     (call-with-input-file file-name get-string-all))
				    #\linefeed)))))))

(if (equal? (basename (car (command-line))) (basename (current-filename)))
    (begin
      (display (solution "day02-input"))
      (newline)))
