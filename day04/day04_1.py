import re

class Card:
    def __init__(self, line):
        self.card_id = int(re.search(r"(?<=Card) +\d+", line).group(0).strip())
        winning_strip = re.search(r"(\d+ +){10}", line).group(0).strip()
        winning_str_numbers = re.split(" +", winning_strip)
        self.winning_numbers = {int(number) for number in winning_str_numbers}
        received_strip = re.search(r"( +\d+){25}", line).group(0).strip()
        received_str_numbers = re.split(" +", received_strip)
        self.received_numbers = {int(number) for number in received_str_numbers}
    def overlap_degree(self):
        return len(self.received_numbers & self.winning_numbers)
    def value(self):
        if self.overlap_degree() == 0:
            return 0
        else:
            return 2**(self.overlap_degree() - 1)

def main():
    with open("day04-input", "r") as f:
        lines = f.readlines()
        sum = 0
        for line in lines:
            card = Card(line)
            sum += card.value()
            """
            print(card.winning_numbers, end="")
            print(" | ", end="")
            print(card.received_numbers, end="")
            print(" | ", end="")
            print(card.received_numbers & card.winning_numbers, end="")
            print(" | ", end="")
            print("%d | %d" % (card.overlap_degree(), card.value()))
            """
    return sum

if __name__ == "__main__":
    print(main())
