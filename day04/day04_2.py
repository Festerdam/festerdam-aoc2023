from day04_1 import *

cards = []

class CardSecond(Card):
    def __init__(self, line):
        super().__init__(line)
        self.card_id = int(re.search(r"(?<=Card) +\d+", line).group(0).strip())
    def get_card_count(self):
        # 1 from self + card count of every gained card
        count = 1
        for card in cards[self.card_id:self.card_id+self.overlap_degree()]:
            count += card.get_card_count()
        return count

def main():
    with open("day04-input", "r") as f:
        lines = f.readlines()
        card_sum = 0
        for line in lines:
            cards.append(CardSecond(line))
        for card in cards:
            card_sum += card.get_card_count()
        return card_sum

print(main())
