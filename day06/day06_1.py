import re
from math import ceil, sqrt

def get_permissible_values(race_time, race_distance):
    # race_distance < (race_time - press_time)×(press_time * press_accel) →
    # → 0 < -(press_time)² + (race_time)×(press_time) - race_distance
    lower_bound = ((race_time - sqrt(race_time**2 - 4 * race_distance))/2)
    upper_bound = ceil((race_time + sqrt(race_time**2 - 4 * race_distance))/2)
    return list(range(ceil(lower_bound), upper_bound))


def main():
    with open("day06-input", "r") as f:
        content = f.read()
        time = re.search(r"Time:.*", content).group(0).removeprefix("Time:").strip()
        times = [int(x) for x in re.split(r" +", time)]
        distance = re.search(r"Distance:.*", content).group(0).removeprefix("Distance:").strip()
        distances = [int(x) for x in re.split(r" +", distance)]
        games = [(times[i], distances[i]) for i in range(len(times))]
        acc = 1
        for game in games:
            acc *= len(get_permissible_values(game[0], game[1]))
        return acc

if __name__ == "__main__":
    print(main())
