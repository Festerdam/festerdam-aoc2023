import re
from math import ceil, sqrt

def get_permissible_values_range_length(race_time, race_distance):
    # race_distance < (race_time - press_time)×(press_time * press_accel) →
    # → 0 < -(press_time)² + (race_time)×(press_time) - race_distance
    lower_bound = ((race_time - sqrt(race_time**2 - 4 * race_distance))/2)
    upper_bound = ceil((race_time + sqrt(race_time**2 - 4 * race_distance))/2)
    return ceil(upper_bound) - ceil(lower_bound)


def main():
    with open("day06-input", "r") as f:
        content = f.read()
        time = re.search(r"Time:.*", content).group(0).removeprefix("Time:").strip()
        time = int(time.replace(" ", ""))
        distance = re.search(r"Distance:.*", content).group(0).removeprefix("Distance:").strip()
        distance = int(distance.replace(" ", ""))
        return get_permissible_values_range_length(time, distance)

if __name__ == "__main__":
    print(main())
