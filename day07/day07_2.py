from day07_1 import *

non_joker_cards = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "Q", "K", "A"]
sorted_cards = ["J", "2", "3", "4", "5", "6", "7", "8", "9", "T", "Q", "K", "A"]


class JokingHand(Hand):
    def __init__(self, hand, bet):
        super().__init__(hand, bet)
        self.sorted_cards = ["J", "2", "3", "4", "5", "6", "7", "8", "9", "T", "Q", "K", "A"]
        self.real_hand = hand
        if "J" in self.hand:
            self.hand = self.best_virtual_hand().hand
    
    def best_virtual_hand(self):
        if self.hand == "J" * 5:
            return Hand("55555", self.bet) # doesn't matter
        joker_count = self.hand.count("J")
        def possible_virtual_hands_beginning_with(beginning, depth):
            possible_hands = []
            # we can safely ignore cards not in the original hand
            possible_cards = list(self.card_frequency().keys())
            possible_cards.remove("J")
            if depth == joker_count:
                return possible_cards
            for card in possible_cards:
                possible_hands += [card + x for x in possible_virtual_hands_beginning_with(card, depth + 1)]
            return possible_hands
        combinations = possible_virtual_hands_beginning_with("", 1)
        processed_combinations = []
        for comb in combinations:
            hand_string =""
            for i in range(joker_count):
                hand_string = self.hand.replace("J", comb[i])
            processed_combinations.append(Hand(hand_string, self.bet))
        return max(processed_combinations)

    def __lt__(self, other):
        if self.get_hand_rank() != other.get_hand_rank():
            return super().__lt__(other)

        for pair in zip(self.real_hand, other.real_hand):
            if pair[0] != pair[1]:
                return self.card_less_than(pair[0], pair[1])
        
        assert False
        

def main():
    global hands
    with open("day07-input", "r") as f:
        lines = f.readlines()
        for line in lines:
            sp = line.strip().split(" ")
            hands.append(JokingHand(sp[0], int(sp[1])))
        hands = sorted(hands)
        s = 0
        for n, hand in enumerate(hands):
            #print(hand.real_hand + ": " + hand.hand + ": " + str(hand.get_hand_rank()))
            s += hand.winnings(n+1)

        return s

print(main())
