hands = []

class Hand:
    def __init__(self, hand, bet):
        self.sorted_cards = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
        self.hand = hand
        self.bet = bet

    def card_frequency(self):
        d = {}
        for l in self.hand:
            if l in d:
                d[l] += 1
            else:
                d[l] = 1
        return d
    
    def is_five_of_a_kind(self):
        return 5 in self.card_frequency().values()
    def is_four_of_a_kind(self):
        return 4 in self.card_frequency().values()
    def is_full_house(self):
        frequency = self.card_frequency()
        return 3 in frequency.values() and 2 in frequency.values()
    def is_three_of_a_kind(self):
        return 3 in self.card_frequency().values()
    def is_two_pair(self):
        count = 0
        for v in self.card_frequency().values():
            if v == 2:
                count += 1
        return count == 2
    def is_one_pair(self):
        return 2 in self.card_frequency().values()

    def get_hand_rank(self):
        if self.is_five_of_a_kind():
            return 7
        if self.is_four_of_a_kind():
            return 6
        if self.is_full_house():
            return 5
        if self.is_three_of_a_kind():
            return 4
        if self.is_two_pair():
            return 3
        if self.is_one_pair():
            return 2
        return 1

    def card_less_than(self, card, other):
        return self.sorted_cards.index(card) < self.sorted_cards.index(other)

    def __lt__(self, other):
        if self.get_hand_rank() != other.get_hand_rank():
            return self.get_hand_rank() < other.get_hand_rank()

        for pair in zip(self.hand, other.hand):
            if pair[0] != pair[1]:
                return self.card_less_than(pair[0], pair[1])

        return False

    def winnings(self, rank):
        return rank * self.bet

def main():
    global hands
    with open("day07-input", "r") as f:
        lines = f.readlines()
        for line in lines:
            sp = line.strip().split(" ")
            hands.append(Hand(sp[0], int(sp[1])))
        hands = sorted(hands)
        s = 0
        for n, hand in enumerate(hands):
            s += hand.winnings(n+1)

        return s

if __name__ == "__main__":
    print(main())
