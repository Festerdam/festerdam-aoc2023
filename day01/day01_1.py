def get_number(line):
    digits = get_digits(line)
    return int(digits[0] + digits[-1])

def get_digits(line):
    digits = ""
    for char in line:
        if char.isnumeric():
            digits += char
    return digits

def main():
    with open("day01-input", "r") as f:
        lines = f.readlines()
        total = 0
        for line in lines:
            total += get_number(line.strip())
        print(total)

if __name__ == "__main__":
    main()
