import re
import day01_1

spelled_out = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
}

def get_number(line):
    digits = ""
    spelled_regex = "one|two|three|four|five|six|seven|eight|nine"
    first = re.search(spelled_regex + r"|\d", line).group(0)
    first = digitify(first)
    last = re.search(spelled_regex[::-1] + r"|\d", line[::-1]).group(0)
    last = digitify(last[::-1])
    return int(str(first) + str(last))

def digitify(x):
    if x.isnumeric():
        return x
    else:
        return spelled_out[x]

if __name__ == "__main__":
    day01_1.get_number = get_number
    day01_1.main()
