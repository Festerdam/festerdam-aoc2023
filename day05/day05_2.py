from day05_1 import *
import re

maps = []

def main():
    with open("day05-input", "r") as f:
        content = f.read()
        first_line = content.split("\n", 1)[0]
        seeds = []
        pairs = [x.strip().split(" ") for x in re.findall(r" \d+ \d+", first_line)]
        pairs = [(int(x[0]), int(x[1])) for x in pairs]
        print(pairs)
        for pair in pairs:
            seeds += list(range(pair[0], pair[0] + pair[1]))
        map_definitions = re.findall(r"\w+-to-\w+ map:\n(?:\d+ \d+ \d+\n)+", content)
        maps = [Map(x) for x in map_definitions]

        transformed_points = seeds
        m = maps[0]
        while m != None:
            transformed_points = m.transform_points(transformed_points)
            m = m.get_dest()
        return min(transformed_points)

print(main())
