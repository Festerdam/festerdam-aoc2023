import re

maps = []

class Map:
    def __init__(self, definition):
        self.src = re.search(r"\w+-to", definition).group(0).strip("-to")
        self.dest = re.search(r"to-\w+", definition).group(0).strip("to-")
        self.range_maps = [RangeMap(x) for x in re.findall(r"\d+ \d+ \d+", definition)]
    def transform_point(self, point):
        r = self.get_range_from_point(point)
        if r != None:
            return r.transform(point)
        else:
            return point
    def transform_points(self, points):
        #print(points)
        return [self.transform_point(point) for point in points]
    def get_range_from_point(self, point):
        for r in self.range_maps:
            if r.in_range(point):
                return r
        return None
    def get_dest(self):
        if self.dest == "location":
            return None
        for m in maps:
            if m.src == self.dest:
                return m
        assert False

class RangeMap:
    def __init__(self, line):
        numbers = [int(x) for x in line.split(" ")]
        dest_start = numbers[0]
        src_start = numbers[1]
        self.length = numbers[2]
        self.src_range = range(src_start, src_start + self.length)
        self.dest_range = range(dest_start, dest_start + self.length)
    def transform(self, point):
        assert point in self.src_range
        return self.dest_range.start + point - self.src_range.start
    def in_range(self, point):
        return point in self.src_range

def main():
    global maps
    with open("day05-input", "r") as f:
        content = f.read()
        seeds = [int(x) for x in re.search(r"(?<=seeds:)(?: \d+)+", content).group(0).strip().split(" ")]
        map_definitions = re.findall(r"\w+-to-\w+ map:\n(?:\d+ \d+ \d+\n)+", content)
        maps = [Map(x) for x in map_definitions]

        transformed_points = seeds
        m = maps[0]
        print(str(len(seeds)) + " seeds")
        while m != None:
            transformed_points = m.transform_points(transformed_points)
            print(m.src + " processed")
            m = m.get_dest()
        return min(transformed_points)

if __name__ == "__main__":
    print(main())
